## 1

```
select c.id  as client_id,
                count(
                        case when price < 1000 then 1 end
                    ) as count1,
                count(
                        case when price > 1000 then 1 end
                    ) as count2
from clients c
         left join orders o on c.id = o.client_id
group by c.id
order by c.id;
```

## 2
```
select o.id, clients.id as client_id, o.price from clients
join lateral (
    select *
    from orders
    where orders.client_id = clients.id
    order by orders.id asc
    offset 2
    limit 1
) o on true
order by clients.id;
```

## 3

```
select *
from clients
         join lateral (
    select o.id, o.client_id, o.price
    from (
             select clients.id as client_id, ids.id
             from clients
                      join lateral (
                 select orders.id
                 from orders
                 where orders.client_id = public.clients.id
                   and price >= 1000
                 limit 1
                 ) ids on true
         ) as ids
             left join orders o on o.client_id = ids.client_id
    where o.id > ids.id
      and o.client_id = clients.id
    order by o.id
        offset 2
    limit 1
    ) r on true;
```